import {createStore, applyMiddleware} from 'redux'
import {routerMiddleware} from 'react-router-redux'
import localforage from 'localforage'

import history from './history'
import rootReducer from './stores'

import {checkAndLoadImages, selectedDomainsUserData} from "./stores/selectedDomains"
import {readArticlesUserData, updateTitle} from "./stores/articles"
import {settingsUserData} from "./stores/settings"

import userDataMiddleware from "./middlewares/userData"
import websocketMiddleware from "./middlewares/websocket"
import beforeAfterActionMiddleware from "./middlewares/beforeAfterAction"

const middlewares = [
    routerMiddleware(history),
    userDataMiddleware({
        storage: localforage,
        userData: [selectedDomainsUserData, readArticlesUserData, settingsUserData]
    }),
    websocketMiddleware(),
    beforeAfterActionMiddleware([updateTitle, checkAndLoadImages]),
]

const compose = process.env.NODE_ENV === 'production'
    ? require('redux').compose
    : require('redux-devtools-extension').composeWithDevTools

export default () => {
    return compose(applyMiddleware(...middlewares))(createStore)(rootReducer)
}