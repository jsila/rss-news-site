
const beforeAfterActionMiddleware = data => store => dispatch => action => {
    data.forEach(ba => {
        const actionTypes = Array.isArray(ba.actionType) ? ba.actionType : [ba.actionType]

        if (!actionTypes.includes(action.type) || ba.before === undefined) {
            return
        }

        const befores = Array.isArray(ba.before) ? ba.before : [ba.before]
        befores.forEach(b => b(store.getState(), dispatch))
    })

    const res = dispatch(action)

    data.forEach(ba => {
        const actionTypes = Array.isArray(ba.actionType) ? ba.actionType : [ba.actionType]

        if (!actionTypes.includes(action.type) || ba.after === undefined) {
            return
        }

        const afters = Array.isArray(ba.after) ? ba.after : [ba.after]
        afters.forEach(b => b(store.getState(), dispatch))
    })

    return res
}

export default beforeAfterActionMiddleware