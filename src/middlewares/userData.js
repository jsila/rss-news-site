import * as R from 'ramda'
import {setUserData} from "../stores/actions"

const userDataMiddleware = ({storage, userData}) => store => {

    Promise.all(userData.map(data => storage.getItem(data.key)))
        .then(vals => {
            const data = userData.reduce((res, d, i) => R.assoc(d.key, vals[i], res), {})
            store.dispatch(setUserData(data))
        })

    return dispatch => action => {
        const result = dispatch(action)

        userData.forEach(data => {
            if (action.type === data.actionType) {
                storage.setItem(data.key, data.onUpdate(store.getState()))
            }
        })

        return result
    }
}

export default userDataMiddleware