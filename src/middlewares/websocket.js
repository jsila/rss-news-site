import moment from "moment"
import config from "../config"

import {addArticle} from "../stores/actions"

const parseArticle = event => {
    const article = JSON.parse(event.data)
    article.date = moment(article.date)
    return article
}

const websocketMiddleware = options => store => dispatch => {
    if (window.WebSocket) {
        const conn = new WebSocket(`ws://${config.domain}/articles`)

        conn.onmessage = event => {
            dispatch(addArticle(parseArticle(event)))
        }

        if (process.env.NODE_ENV !== 'production') {
            conn.onerror = console.log
        }
    }

    return dispatch
}

export default websocketMiddleware