import {combineReducers} from 'redux'
import {routerReducer as router} from 'react-router-redux'

import articles from './articles'
import domains from './domains'
import selectedDomains from './selectedDomains'
import settings from "./settings"

export default combineReducers({
    articles,
    domains,
    selectedDomains,
    settings,
    router
})