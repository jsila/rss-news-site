import {handleActions} from 'redux-actions'
import {createSelector} from 'reselect'
import * as R from 'ramda'

import {ADD_ARTICLE} from "./actions"

const initialState = []

const handlers = {
    [ADD_ARTICLE](state, action) {
        const {article} = action.payload
        if (article.domain in state) {
            return state
        }
        const data = {
            site: article.site,
            domain: article.domain,
        }
        return R.assoc(article.domain, data, state)
    }
}

export default handleActions(handlers, initialState)

export const getDomains = (state, ownProps) => state.domains

export const getDomainOptions = createSelector(
    getDomains,
    domains => R.values(domains).map(d => ({
        key: d.domain,
        text: d.site,
        value: d.domain
    }))
)