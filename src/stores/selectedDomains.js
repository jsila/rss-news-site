import {handleActions} from 'redux-actions'
import * as R from 'ramda'

import {getDomains} from "./domains";
import {MARK_ARTICLE_AS_READ, SELECT_DOMAINS, SET_USER_DATA} from "./actions";

const initialState = []

const handlers = {
    [SELECT_DOMAINS](state, action) {
        return action.payload
    },
    [SET_USER_DATA](state, action) {
        return action.payload.selectedDomains || []
    }
}

export default handleActions(handlers, initialState)

export const getSelectedDomains = (state, ownProps) => {
    const domains = getDomains(state)
    return state.selectedDomains.length ? R.pick(state.selectedDomains, domains) : domains
}

export const selectedDomainsUserData = {
    actionType: SELECT_DOMAINS,
    key: 'selectedDomains',
    onUpdate: state => state.selectedDomains
}

export const checkAndLoadImages = {
    actionType: [SELECT_DOMAINS, MARK_ARTICLE_AS_READ],
    after: () => {
        const top = document.documentElement.scrollTop || document.body.scrollTop
        window.scrollTo(0, top+1)
        window.scrollTo(0, top)
    }
}