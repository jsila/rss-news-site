import {handleActions} from 'redux-actions'
import * as R from 'ramda'

import {UPDATE_SETTINGS, SET_USER_DATA} from "./actions";

const initialState = {
    markAsRead: true
}

const handlers = {
    [UPDATE_SETTINGS](state, action) {
        const {field, value} = action.payload
        return R.assoc(field, value, state)
    },
    [SET_USER_DATA](state, action) {
        const settings = R.isEmpty(action.payload.settings)
            ? initialState
            : R.pick(R.keys(initialState), action.payload.settings)
        return R.merge(state, settings)
    }
}

export default handleActions(handlers, initialState)

export const getSettings = (state, ownProps) => state.settings

export const settingsUserData = {
    actionType: UPDATE_SETTINGS,
    key: 'settings',
    onUpdate: getSettings
}