import {handleActions} from 'redux-actions'
import {createSelector} from 'reselect'
import * as R from 'ramda'

import {getSelectedDomains} from "./selectedDomains";
import {ADD_ARTICLE, MARK_ARTICLE_AS_READ, SET_USER_DATA} from "./actions";

const initialState = {
    data: {},
    urls: [],
    read: []
}

const handlers = {
    [ADD_ARTICLE](state, action) {
        const {article} = action.payload

        return R.evolve({
            data: R.assoc(article.url, article),
            urls: R.append(article.url)
        }, state)
    },
    [MARK_ARTICLE_AS_READ](state, action) {
        const {url, read} = action.payload
        return R.evolve({
            read: read ? R.append(url) : R.reject(R.equals(url))
        }, state)
    },
    [SET_USER_DATA](state, action) {
        return R.assoc('read', action.payload.readArticles || [], state)
    }

}

export default handleActions(handlers, initialState)

export const getArticles = state => state.articles

export const getReadArticles = createSelector(
    getArticles,
    R.prop('read')
)

export const getArticle = (state, ownProps) => createSelector(
    getArticles,
    R.path(['data', ownProps.url])
)(state)

export const getArticlesFromSelectedDomains = (state, ownProps) => createSelector(
    getReadArticles,
    state => state.selectedDomains,
    (read, selectedDomains) => R.compose(
            R.filter(url => ownProps.read ? read.includes(url) : !read.includes(url)),
            R.map(R.prop('url')),
            R.sort((a, b) => b.date.diff(a.date)),
            R.filter(a => R.isEmpty(selectedDomains) || selectedDomains.includes(a.domain)),
            R.values
        )(state.articles.data)
)(state)

export const isRead = (state, ownProps) => createSelector(
    getReadArticles,
    R.contains(ownProps.url)
)(state)

export const getArticlesByDomain = createSelector(
    getArticles,
    R.compose(
        R.groupBy(R.prop('domain')),
        R.values,
        R.prop('data')
    )
)

export const getArticlesCountFromSelectedDomains = state => createSelector(
    getReadArticles,
    getSelectedDomains,
    getArticlesByDomain,
    (readArticles, selectedDomains, articlesByDomain) => {
        const selectedUrls = [].concat.apply([], Object.keys(selectedDomains).map(d => articlesByDomain[d].map(R.prop('url'))))
        const readUrls = readArticles.filter(u => selectedUrls.includes(u))

        return {
            read: readUrls.length,
            unread: readUrls.length ? (selectedUrls.length - readUrls.length) : selectedUrls.length
        }
    }
)(state)

/*
 * save read articles to offline storage
 */
export const readArticlesUserData = {
    actionType: MARK_ARTICLE_AS_READ,
    key: 'readArticles',
    onUpdate: getReadArticles
}

/*
 * execute before/after action is dispatched
 */
export const updateTitle = {
    actionType: [ADD_ARTICLE, MARK_ARTICLE_AS_READ],
    after: state => {
        const unread = getArticlesCountFromSelectedDomains(state).unread
        document.title = `RSS News (${unread})`
    },
}