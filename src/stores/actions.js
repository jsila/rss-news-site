import {createAction} from "redux-actions";

export const ADD_ARTICLE = 'ADD_ARTICLE'
export const MARK_ARTICLE_AS_READ = 'MARK_ARTICLE_AS_READ'

export const SELECT_DOMAINS = 'SELECT_DOMAINS'

export const UPDATE_SETTINGS = 'UPDATE_SETTINGS'

export const SET_USER_DATA = 'SET_USER_DATA'

export const addArticle = createAction(ADD_ARTICLE, (article) => ({article}))

export const markArticleAsRead = createAction(MARK_ARTICLE_AS_READ, (url, read) => ({url, read}))

export const selectDomains = createAction(SELECT_DOMAINS, domains => domains || [])

export const updateSettings = createAction(UPDATE_SETTINGS, (field, value) => ({field, value}))

export const setUserData = createAction(SET_USER_DATA)