import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'
import {ConnectedRouter} from 'react-router-redux'

import registerServiceWorker from './registerServiceWorker'
import history from './history'
import configureStore from './configureStore'

import App from './components/App'

import './index.css'

const store = configureStore()

const Root = (
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <App />
        </ConnectedRouter>
    </Provider>
)

ReactDOM.render(Root, document.getElementById('root'))
registerServiceWorker()