const prod = {
  domain: '192.168.1.5:8081'
}

const dev = {
  domain: 'localhost:8080'
}

export default process.env.NODE_ENV === 'production' ? prod : dev