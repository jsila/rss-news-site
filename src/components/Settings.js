import React from 'react'
import * as R from 'ramda'
import {Checkbox} from 'semantic-ui-react'
import {connect} from 'react-redux'

import {updateSettings} from '../stores/actions'
import {getSettings} from "../stores/settings"

const Settings = ({settings, updateSettings}) => {
    return (
        <div className="ui segment">
            <form className="ui form">
                <div className="field">
                    <Checkbox label="After clicking a link to article, mark it as read."
                              checked={settings.markAsRead}
                              onChange={(event, data) => updateSettings('markAsRead', data.checked)} />
                </div>
            </form>
        </div>
    )
}

const mapStateToProps = R.applySpec({
    settings: getSettings
})

export default connect(mapStateToProps, {updateSettings})(Settings)