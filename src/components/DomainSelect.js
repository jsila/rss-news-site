import React from 'react'
import * as R from "ramda"
import {connect} from 'react-redux'
import {Dropdown} from 'semantic-ui-react'

import {selectDomains} from "../stores/actions"
import {getDomainOptions} from "../stores/domains"

const DomainSelect = ({domainOptions, selectedDomains, selectDomains}) => {
    return (
        <Dropdown placeholder='Filter by sources'
                  fluid multiple selection
                  options={domainOptions}
                  value={selectedDomains}
                  onChange={(event, data) => selectDomains(data.value)}
        />
    )
}

const mapStateToProps = R.applySpec({
    domainOptions: getDomainOptions,
    selectedDomains: R.prop('selectedDomains')
})


export default connect(mapStateToProps, {selectDomains})(DomainSelect)