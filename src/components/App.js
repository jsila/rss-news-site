import React from 'react'
import {connect} from "react-redux"
import * as R from "ramda"
import {NavLink, Route} from 'react-router-dom'

import Articles from './Articles'
import Settings from "./Settings"
import DomainSelect from './DomainSelect'

import './App.css'

import {getArticlesCountFromSelectedDomains} from "../stores/articles"

const App = props => {
    const {articlesCount: {read, unread}} = props

    return (
        <div className="ui grid container">
            <div className="centered row">
                <div className="twelve wide column">
                    <DomainSelect />

                    <div className="ui stackable secondary pointing menu">
                        <NavLink exact to="/" className="item"><i className="list layout icon"/> Unread ({unread})</NavLink>
                        <NavLink to="/read" className="item"><i className="checkmark box icon"/> Read ({read})</NavLink>
                        <div className="right menu">
                            <NavLink to="/settings" className="ui item"><i className="settings icon"/> Settings</NavLink>
                        </div>
                    </div>

                    <Route exact path="/" component={Articles} />
                    <Route path="/read" render={() => <Articles read />} />
                    <Route path="/settings" component={Settings}/>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = R.applySpec({
    articlesCount: getArticlesCountFromSelectedDomains,
})

export default connect(mapStateToProps)(App)