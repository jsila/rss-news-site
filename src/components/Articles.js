import React from 'react'
import {connect} from 'react-redux'
import * as R from "ramda"

import Article from './Article'
import './Articles.css'
import {getArticlesFromSelectedDomains} from "../stores/articles"

const Articles = ({articles}) => {
    return (
        <div className="ui divided items">
            {articles.map(url => <Article url={url} key={url} />)}
        </div>
    )
}

const mapStateToProps = R.applySpec({
    articles: getArticlesFromSelectedDomains
})

export default connect(mapStateToProps)(Articles)