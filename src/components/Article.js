import React from 'react'
import * as R from 'ramda'
import LazyLoad from 'react-lazyload'
import {connect} from 'react-redux'
import classnames from 'classnames'

import {getArticle, isRead} from "../stores/articles"
import {markArticleAsRead} from "../stores/actions"

import ArticleImage from "./ArticleImage"

import './Article.css'

const Article = ({article, markArticleAsRead, read, visitedArticlesMarkedAsRead}) => {
    const onArticleRead = () => markArticleAsRead(article.url, !read)
    const onArticleVisit = visitedArticlesMarkedAsRead ? () => markArticleAsRead(article.url, true) : undefined

    const iconClassname = classnames('icon', {
        "checkmark box": !read,
        remove: read
    })

    return (
        <div className="article item">
            <LazyLoad>
                <ArticleImage article={article} onArticleVisit={onArticleVisit} />
            </LazyLoad>
            <div className="content">
                <a className="header" href={article.url} target="_blank" onClick={onArticleVisit}>{article.title}</a>
                <div className="meta">
                    <span>{article.date.fromNow()}</span>
                    <span>&middot;</span>
                    <span><a href={`//${article.domain}`}>{article.domain}</a></span>
                    <span>&middot;</span>
                    <span><i className={iconClassname} style={{cursor: 'pointer'}} onClick={onArticleRead} /><span className="checkmark-help-text">Mark as {read ? 'unread' : 'read'}</span></span>
                </div>
                <div className="description">
                    <p dangerouslySetInnerHTML={{__html: article.description}} />
                </div>
                <div className="extra">
                    {article.categories && article.categories.map(c => <span key={c} className="ui label" dangerouslySetInnerHTML={{__html: c}}  />)}
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = R.applySpec({
    article: getArticle,
    read: isRead,
    visitedArticlesMarkedAsRead: state => state.settings.markAsRead
})

export default connect(mapStateToProps, {markArticleAsRead})(Article)