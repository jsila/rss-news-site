import React from 'react'
import config from '../config'

const createImageSRC = image => `//${config.domain}/images/${image}`

const ArticleImage = ({article, onArticleVisit}) => (
    <div className="ui medium image">
        {article.fresh && <a className="ui yellow ribbon label">NEW</a>}
        <a href={article.url} target="_blank" onClick={onArticleVisit}>
            <img src={createImageSRC(article.image)} alt={article.title}/>
        </a>
    </div>
)

export default ArticleImage